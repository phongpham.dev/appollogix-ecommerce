CREATE DATABASE IF NOT EXISTS `ecommerce` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `ecommerce`;

CREATE TABLE IF NOT EXISTS `purchase_order` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `customer_id` varchar(255) NOT NULL DEFAULT '',
    `status` smallint(4) NOT NULL DEFAULT '1',
    `estimate_products` JSON DEFAULT NULL,
    `has_vat` tinyint(1) NOT NULL DEFAULT '0',
    `total_price` double NOT NULL,
    `created_time` bigint(20) NOT NULL DEFAULT '0',
    `updated_time` bigint(20) NOT NULL DEFAULT '0',
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;