package appollogix.ecommerce.service;


import appollogix.ecommerce.domain.entity.PurchaseOrder;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface PurchaseOrderService {
    CompletableFuture<PurchaseOrder> add(PurchaseOrder purchaseOrder);

    CompletableFuture<List<PurchaseOrder>> getAll();
}
