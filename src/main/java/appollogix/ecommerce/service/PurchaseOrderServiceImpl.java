package appollogix.ecommerce.service;

import appollogix.ecommerce.domain.entity.PurchaseOrder;
import appollogix.ecommerce.repository.PurchaseOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@AllArgsConstructor
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private final PurchaseOrderRepository purchaseOrderRepository;

    @Override
    public CompletableFuture<PurchaseOrder> add(PurchaseOrder purchaseOrder) {
        return CompletableFuture.supplyAsync(() -> purchaseOrderRepository.save(purchaseOrder));
    }

    @Override
    public CompletableFuture<List<PurchaseOrder>> getAll() {
        return CompletableFuture.supplyAsync(purchaseOrderRepository::findAll);
    }
}
