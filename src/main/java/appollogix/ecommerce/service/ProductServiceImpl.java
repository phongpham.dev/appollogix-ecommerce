package appollogix.ecommerce.service;

import appollogix.ecommerce.domain.Pageable;
import appollogix.ecommerce.domain.Product;
import appollogix.ecommerce.domain.SearchReq;
import appollogix.ecommerce.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public CompletableFuture<Pageable<Product>> search(SearchReq searchReq) {
        return productRepository.search(searchReq);
    }

}
