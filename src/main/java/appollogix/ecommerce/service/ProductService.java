package appollogix.ecommerce.service;

import appollogix.ecommerce.domain.Pageable;
import appollogix.ecommerce.domain.Product;
import appollogix.ecommerce.domain.SearchReq;

import java.util.concurrent.CompletableFuture;

public interface ProductService {
    CompletableFuture<Pageable<Product>> search(SearchReq searchReq);
}
