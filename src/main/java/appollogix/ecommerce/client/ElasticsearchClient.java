package appollogix.ecommerce.client;

import lombok.AllArgsConstructor;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.PlainActionFuture;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.elasticsearch.client.security.RefreshPolicy;
import org.elasticsearch.common.collect.Tuple;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ElasticsearchClient {
    RestHighLevelClient getClient();

    CompletableFuture<BulkResponse> mIndex(String indexName,
                                           List<Tuple<String, String>> docs,
                                           Boolean refresh, Boolean onlyNotExist);

    Boolean createIndexWithSettingAndMapping(String indexName, String setting, String mappings);

    Boolean updateMapping(String indexName, String mapping);


    @AllArgsConstructor
    class ESRestClient implements ElasticsearchClient {

        RestHighLevelClient client;

        @Override
        public RestHighLevelClient getClient() {
            return client;
        }

        @Override
        public CompletableFuture<BulkResponse> mIndex(String indexName, List<Tuple<String, String>> docs,
                                                      Boolean refresh, Boolean onlyNotExist) {
            BulkRequest request = new BulkRequest();
            request.setRefreshPolicy(refresh ? RefreshPolicy.IMMEDIATE.getValue() : RefreshPolicy.NONE.getValue());
            docs.forEach(tuple -> request.add(
                            new IndexRequest(indexName)
                                    .id(tuple.v1())
                                    .source(tuple.v2(), XContentType.JSON)
                                    .opType(onlyNotExist ? DocWriteRequest.OpType.CREATE : DocWriteRequest.OpType.INDEX)
                    )
            );
            var cf = new CompletableFuture<BulkResponse>();
            client.bulkAsync(request, RequestOptions.DEFAULT,
                    ActionListener.wrap(cf::complete, cf::completeExceptionally)
            );
            return cf;
        }

        @Override
        public Boolean createIndexWithSettingAndMapping(String indexName, String setting, String mappings) {
            try {
                var isExist = client.indices().exists(new GetIndexRequest(indexName), RequestOptions.DEFAULT);
                if (!isExist) {
                    CreateIndexRequest request = new CreateIndexRequest(indexName);
                    if (!setting.isEmpty()) request.settings(setting, XContentType.JSON);
                    if (!mappings.isEmpty()) request.mapping(mappings, XContentType.JSON);
                    CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
                    return createIndexResponse.isAcknowledged();
                }
                return false;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Boolean updateMapping(String indexName, String mapping) {
            PutMappingRequest request = new PutMappingRequest(indexName);
            request.source(mapping, XContentType.JSON);
            try {
                AcknowledgedResponse acknowledgedResponse = client.indices().putMapping(request, RequestOptions.DEFAULT);
                return acknowledgedResponse.isAcknowledged();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
