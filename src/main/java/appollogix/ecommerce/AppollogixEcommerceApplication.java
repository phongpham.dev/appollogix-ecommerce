package appollogix.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppollogixEcommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppollogixEcommerceApplication.class, args);
	}

}
