package appollogix.ecommerce.config;

import appollogix.ecommerce.client.ElasticsearchClient;
import appollogix.ecommerce.repository.ProductRepository;
import appollogix.ecommerce.utils.JsonHelper;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.collect.Tuple;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.Spliterators;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.StreamSupport;

@Configuration
public class ApplicationConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    public ElasticsearchClient elasticsearchClient(
            @Value("${elasticsearch.host}") String host,
            @Value("${elasticsearch.port}") int port
    ) throws IOException {
        logger.info("Connect to elasticsearchClient host:port " + host + ":" + port);
        var restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost(host, port)));
        ElasticsearchClient esClient = new ElasticsearchClient.ESRestClient(restHighLevelClient);
        var db = new ClassPathResource("databases/elasticsearch/es_mapping.json").getFile();
        var jsonTree = JsonHelper.getObjectMapper().readTree(db);
        jsonTree.fields().forEachRemaining(v -> {
            var settings = v.getValue().at("/settings").toString();
            var mappings = v.getValue().at("/mappings").toString();
            esClient.createIndexWithSettingAndMapping(v.getKey(), settings, mappings);
            esClient.updateMapping(v.getKey(), mappings);
        });
        return esClient;
    }

    @Bean
    public ProductRepository productRepository(
            @Value("${elasticsearch.product_index_name}") String indexName,
            ElasticsearchClient elasticsearchClient
    ) {
        return new ProductRepository.ProductRepositoryImpl(indexName, elasticsearchClient);
    }

    @Bean
    @Profile({"default", "ci"})
    public Void initESData(ElasticsearchClient esClient,
                           @Autowired ResourceLoader resourceLoader) throws IOException {
        var resource = resourceLoader.getResource("classpath:databases/elasticsearch/data.json").getFile();
        var fileAsString = FileUtils.readFileToString(resource, "utf-8");
        var jsonTree = JsonHelper.getObjectMapper().readTree(fileAsString);

        jsonTree.fields().forEachRemaining(f -> {
            try {
                esClient.getClient().deleteByQuery(
                        new DeleteByQueryRequest(f.getKey()).setQuery(QueryBuilders.matchAllQuery()), RequestOptions.DEFAULT
                );

                var spliterator = Spliterators.spliteratorUnknownSize(f.getValue().at("/data").elements(), 0);
                var dataForTest = StreamSupport.stream(spliterator, false).map(source -> Tuple.tuple(UUID.randomUUID().toString(), source.toString())).toList();
                esClient.mIndex(f.getKey(), dataForTest, false, true).get();

            } catch (IOException | InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        });
        return null;
    }

}
