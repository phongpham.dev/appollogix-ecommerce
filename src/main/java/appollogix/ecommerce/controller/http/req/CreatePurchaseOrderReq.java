package appollogix.ecommerce.controller.http.req;

import appollogix.ecommerce.domain.EstimateProduct;
import appollogix.ecommerce.domain.entity.PurchaseOrder;
import appollogix.ecommerce.domain.enumeration.Status;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;

import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class CreatePurchaseOrderReq {

    @NotBlank
    private String customerId;

    @Valid
    private List<EstimateProductReq> products = Collections.emptyList();

    @Min(value = 1, message = "totalPrice must be > 0")
    private Double totalPrice;

    private Boolean hasVat = false;

    public PurchaseOrder toCreatePurchaseOrder() {
        var currTime = System.currentTimeMillis();
        return PurchaseOrder.builder()
                .customerId(customerId)
                .status(Status.AWAITING_PAYMENT.getId())
                .estimateProducts(products.stream().map(EstimateProductReq::toEstimateProduct).toList())
                .hasVat(hasVat)
                .totalPrice(totalPrice)
                .createdTime(currTime)
                .updatedTime(currTime)
                .build();
    }
}
