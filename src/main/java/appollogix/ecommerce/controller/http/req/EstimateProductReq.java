package appollogix.ecommerce.controller.http.req;

import appollogix.ecommerce.domain.EstimateProduct;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class EstimateProductReq {

    @NotBlank
    private String productId;

    @NotNull
    @Min(value = 1, message = "quantity must be > 0")
    private Integer quantity;

    @NotNull
    @Min(value = 1, message = "unitPrice must be > 0")
    private Double unitPrice;

    @NotNull
    @Min(value = 1, message = "lineTotal must be > 0")
    private Double lineTotal;

    private Optional<String> note = Optional.empty();

    public EstimateProduct toEstimateProduct() {
        return new EstimateProduct(productId, quantity, unitPrice, lineTotal, note);
    }
}
