package appollogix.ecommerce.controller.http.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AdviceExceptionHandler {
    @ExceptionHandler(value = {Throwable.class})
    public ResponseEntity<ErrorResponse> commonExceptionMapping(Throwable ex) {
        ErrorResponse errorResponse = new ErrorResponse("internal_error", ex.getMessage());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }

    @AllArgsConstructor
    @Getter
    @Setter
    public static class ErrorResponse {
        private String error;

        @JsonProperty("error_msg")
        private String errorMsg = null;
    }
}
