package appollogix.ecommerce.controller.http;

import appollogix.ecommerce.controller.http.req.CreatePurchaseOrderReq;
import appollogix.ecommerce.domain.entity.PurchaseOrder;
import appollogix.ecommerce.service.PurchaseOrderService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
@RestController
@RequestMapping("/order")
public class PurchaseOrderController {

    private final PurchaseOrderService purchaseOrderService;

    @PostMapping
    public CompletableFuture<PurchaseOrder> create(@RequestBody @Valid CreatePurchaseOrderReq request) {
        return purchaseOrderService.add(request.toCreatePurchaseOrder());
    }

    @GetMapping
    public CompletableFuture<List<PurchaseOrder>> getAll() {
        return purchaseOrderService.getAll();
    }
}
