package appollogix.ecommerce.controller.http;

import appollogix.ecommerce.domain.Pageable;
import appollogix.ecommerce.domain.Product;
import appollogix.ecommerce.domain.SearchReq;
import appollogix.ecommerce.service.ProductService;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
@RestController
@Validated
@RequestMapping("/product")
public class ProductController {

    ProductService productService;

    @GetMapping("/search")
    public CompletableFuture<Pageable<Product>> search(
            @RequestParam(value = "page", defaultValue = "1") @Min(1) int page,
            @RequestParam(value = "size", defaultValue = "20") @Min(0) @Max(100) int size,
            @RequestParam(value = "keyword", required = false) Optional<String> keyword,
            @RequestParam(value = "name", required = false) Optional<String> name,
            @RequestParam(value = "image", required = false) Optional<String> image,
            @RequestParam(value = "from_price", required = false) Optional<Double> fromPrice,
            @RequestParam(value = "to_price", required = false) Optional<Double> toPrice,
            @RequestParam(value = "colors", defaultValue = "", required = false) List<String> colors,
            @RequestParam(value = "category_ids", defaultValue = "") List<String> categoryIds
    ) {
        SearchReq searchReq = SearchReq.builder()
                .from((page - 1) * size).size(size)
                .keyword(keyword)
                .name(name)
                .image(image)
                .fromPrice(fromPrice).toPrice(toPrice)
                .colors(colors)
                .categoryIds(categoryIds)
                .build();
        return productService.search(searchReq);
    }

}
