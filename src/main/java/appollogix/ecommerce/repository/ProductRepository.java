package appollogix.ecommerce.repository;

import appollogix.ecommerce.client.ElasticsearchClient;
import appollogix.ecommerce.domain.Pageable;
import appollogix.ecommerce.domain.Product;
import appollogix.ecommerce.domain.SearchReq;
import appollogix.ecommerce.utils.JsonHelper;
import lombok.AllArgsConstructor;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static appollogix.ecommerce.domain.Product.ProductField.*;

public interface ProductRepository {

    CompletableFuture<Pageable<Product>> search(SearchReq searchReq);

    @AllArgsConstructor
    class ProductRepositoryImpl implements ProductRepository {

        private final String indexName;

        private final ElasticsearchClient esClient;

        private final Logger logger = LoggerFactory.getLogger(getClass());

        @Override
        public CompletableFuture<Pageable<Product>> search(SearchReq req) {
            BoolQueryBuilder searchQuery = QueryBuilders.boolQuery();

            req.getKeyword().ifPresent(k -> {
                var _q = QueryBuilders.boolQuery()
                        .should(QueryBuilders.matchQuery(NAME, k).operator(Operator.AND))
                        .should(QueryBuilders.wildcardQuery(NAME, String.format("*%s*", k)))
                        .should(QueryBuilders.termsQuery(NAME + ".raw", k));
                        //.should(QueryBuilders.wildcardQuery(NAME + ".raw", String.format("*%s*", k)));
                searchQuery.must(_q);
            });

            req.getName().ifPresent(v -> searchQuery.must(QueryBuilders.termsQuery(NAME + ".raw", v)));
            req.getImage().ifPresent(v -> searchQuery.must(QueryBuilders.termQuery(IMAGE, v)));

            if (req.getFromPrice().isPresent() && req.getToPrice().isPresent()) {
                searchQuery.must(
                        QueryBuilders.rangeQuery(PRICE)
                                .gte(req.getFromPrice().get())
                                .lt(req.getToPrice().get())
                );
            } else if (req.getFromPrice().isPresent()) {
                searchQuery.must(QueryBuilders.rangeQuery(PRICE).gte(req.getFromPrice().get()));
            } else if (req.getToPrice().isPresent()) {
                searchQuery.must(QueryBuilders.rangeQuery(PRICE).lt(req.getToPrice().get()));
            }

            if (Objects.nonNull(req.getColors()) && !req.getColors().isEmpty()) {
                searchQuery.must(QueryBuilders.termsQuery(COLOR, req.getColors()));
            }

            if (Objects.nonNull(req.getCategoryIds()) && !req.getCategoryIds().isEmpty()) {
                searchQuery.must(QueryBuilders.termsQuery(CATEGORIES + ".id", req.getCategoryIds()));
            }

            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                    .from(req.getFrom())
                    .size(req.getSize());
            sourceBuilder.query(searchQuery);

            SearchRequest searchReq = new SearchRequest();
            searchReq.source(sourceBuilder);

            logger.info("elasticsearch query: " + searchReq.source().toString());

            var cf = new CompletableFuture<SearchResponse>();
            esClient.getClient().searchAsync(
                    searchReq, RequestOptions.DEFAULT,
                    ActionListener.wrap(cf::complete, cf::completeExceptionally)
            );

            return cf.thenApply(resp -> {
                var tasks = Arrays.stream(resp.getHits().getHits())
                        .map(h -> JsonHelper.fromJson(h.getSourceAsString(), Product.class))
                        .toList();
                return new Pageable<Product>(
                        resp.getHits().getTotalHits().value,
                        tasks
                );
            });
        }
    }
}
