package appollogix.ecommerce.domain;

import lombok.*;

import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Pageable<T> {
    private Long total;
    private List<T> data = Collections.emptyList();
}
