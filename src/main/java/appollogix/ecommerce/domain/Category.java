package appollogix.ecommerce.domain;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Category {
    private String id;
    private String name;
}
