package appollogix.ecommerce.domain;

import lombok.*;

import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Product {
    private String id;
    private String name;
    private Optional<String> description;
    private String image;
    private Double price;
    private List<String> colors;
    private Long createdTime;
    private Long updatedTime;
    private List<Category> categories;

    public static class ProductField {
        public final static String ID = "id";
        public final static String NAME = "name";
        public final static String IMAGE = "image";
        public final static String PRICE = "price";
        public final static String COLOR = "colors";
        public final static String CREATED_TIME = "created_time";
        public final static String UPDATED_TIME = "updated_time";
        public final static String CATEGORIES = "categories";
    }
}
