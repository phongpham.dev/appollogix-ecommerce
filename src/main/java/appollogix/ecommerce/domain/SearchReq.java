package appollogix.ecommerce.domain;

import lombok.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SearchReq {
    private int from;
    private int size;
    private Optional<String> keyword;
    private Optional<String> name;
    private Optional<String> image;
    private Optional<Double> fromPrice;
    private Optional<Double> toPrice;
    private List<String> colors;
    private List<String> categoryIds;
}
