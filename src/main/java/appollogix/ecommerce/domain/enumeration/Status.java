package appollogix.ecommerce.domain.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
public enum Status {

    AWAITING_PAYMENT(1, "awaiting_payment"),
    SHIPPING(2, "shipping"),
    COMPLETED(3, "completed");

    private final Integer id;
    private final String name;

    public static Optional<Status> opt(Integer id) {
        return Arrays.stream(values())
                .filter(v -> v.id.equals(id))
                .findFirst();
    }

    public static Status get(Integer id) {
        return opt(id).orElseThrow();
    }
}
