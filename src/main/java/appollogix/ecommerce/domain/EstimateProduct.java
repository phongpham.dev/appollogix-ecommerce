package appollogix.ecommerce.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EstimateProduct {
    private String productId;
    private Integer quantity;
    private Double unitPrice;
    private Double lineTotal;
    private Optional<String> note;
}
