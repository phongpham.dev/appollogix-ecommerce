package appollogix.ecommerce.domain.entity;

import appollogix.ecommerce.domain.EstimateProduct;
import appollogix.ecommerce.utils.JsonHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "purchase_order")
public class PurchaseOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "status")
    private Integer status;

    @Column(name = "estimate_products")
    @Convert(converter = Converter.EstimateProductsConverter.class)
    private List<EstimateProduct> estimateProducts;

    @Column(name = "has_vat")
    private Boolean hasVat;

    @Column(name = "total_price")
    private Double totalPrice;

    @Column(name = "created_time")
    private Long createdTime;

    @Column(name = "updated_time")
    private Long updatedTime;

    public static class Converter {

        static public class EstimateProductsConverter implements AttributeConverter<List<EstimateProduct>, String> {
            @Override
            public String convertToDatabaseColumn(List<EstimateProduct> attribute) {
                if (!Objects.isNull(attribute)) return JsonHelper.toJson(attribute);
                return null;
            }

            @Override
            public List<EstimateProduct> convertToEntityAttribute(String dbData) {
                if (!Objects.isNull(dbData)) {
                    try {
                        return JsonHelper.getObjectMapper().readValue(dbData, new TypeReference<List<EstimateProduct>>() {
                        });
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                }
                return Collections.emptyList();
            }
        }


    }
}
