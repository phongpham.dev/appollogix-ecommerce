package appollogix.ecommerce;

import appollogix.ecommerce.controller.http.req.CreatePurchaseOrderReq;
import appollogix.ecommerce.controller.http.req.EstimateProductReq;

import java.util.List;
import java.util.Optional;

public class DataInitializer {

    public static CreatePurchaseOrderReq createPurchaseOrderReq() {
        var estimateProductReq = List.of(
                EstimateProductReq.builder()
                        .productId("product id")
                        .quantity(5)
                        .unitPrice(10d)
                        .lineTotal(50d)
                        .note(Optional.of("estimate product note"))
                        .build()
        );

        return CreatePurchaseOrderReq.builder()
                .customerId("phongpq")
                .hasVat(true)
                .products(estimateProductReq)
                .totalPrice(50d)
                .build();
    }
}
