package appollogix.ecommerce.config;

import appollogix.ecommerce.client.ElasticsearchClient;
import appollogix.ecommerce.repository.ProductRepository;
import appollogix.ecommerce.repository.PurchaseOrderRepository;
import appollogix.ecommerce.service.ProductService;
import appollogix.ecommerce.service.ProductServiceImpl;
import appollogix.ecommerce.service.PurchaseOrderService;
import appollogix.ecommerce.service.PurchaseOrderServiceImpl;
import appollogix.ecommerce.utils.JsonHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;

@TestConfiguration
@Import(JpaConfigTest.class)
public class ApplicationConfigTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    public ElasticsearchClient elasticsearchClient(
            @Value("${elasticsearch.host}") String host,
            @Value("${elasticsearch.port}") int port
    ) throws IOException {
        logger.info("Connect to elasticsearchClient host:port " + host + ":" + port);
        var restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost(host, port)));
        ElasticsearchClient esClient = new ElasticsearchClient.ESRestClient(restHighLevelClient);
        var db = new ClassPathResource("databases/elasticsearch/es_mapping.json").getFile();
        var jsonTree = JsonHelper.getObjectMapper().readTree(db);
        jsonTree.fields().forEachRemaining(v -> {
            var settings = v.getValue().at("/settings").toString();
            var mappings = v.getValue().at("/mappings").toString();
            esClient.createIndexWithSettingAndMapping(v.getKey(), settings, mappings);
            esClient.updateMapping(v.getKey(), mappings);
        });
        return esClient;
    }

    @Bean
    public ProductRepository productRepository(
            @Value("${elasticsearch.product_index_name}") String indexName,
            ElasticsearchClient elasticsearchClient
    ) {
        return new ProductRepository.ProductRepositoryImpl(indexName, elasticsearchClient);
    }

    @Bean
    public ProductService productService(ProductRepository productRepository) {
        return new ProductServiceImpl(productRepository);
    }

    @Bean
    public PurchaseOrderService purchaseOrderService(@Autowired PurchaseOrderRepository purchaseOrderRepository) {
        return new PurchaseOrderServiceImpl(purchaseOrderRepository);
    }

}
