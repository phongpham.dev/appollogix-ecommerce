package appollogix.ecommerce.controller;


import appollogix.ecommerce.DataInitializer;
import appollogix.ecommerce.config.ApplicationConfigTest;
import appollogix.ecommerce.controller.http.PurchaseOrderController;
import appollogix.ecommerce.utils.JsonHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(
        PurchaseOrderController.class
)
@Import(ApplicationConfigTest.class)
@Sql(scripts = {"/databases/mysql/schema.sql"})
@ActiveProfiles({"default"})
public class PurchaseOrderControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void create_Simple_Success() throws Exception {
        var data = DataInitializer.createPurchaseOrderReq();
        var jsonForCreate = JsonHelper.toJson(data);
        var mvcResult = mockMvc.perform(
                        post("/order")
                                .content(jsonForCreate)
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpectAll(
                        status().isOk(),
                        request().asyncStarted()
                )
                .andReturn();

        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andDo(print())
                .andExpectAll(
                        jsonPath("customer_id").value(data.getCustomerId()),
                        jsonPath("has_vat").value(data.getHasVat()),
                        jsonPath("total_price").value(data.getTotalPrice()),
                        jsonPath("estimate_products.[0].quantity").value(data.getProducts().get(0).getQuantity())
                )
        ;
    }
}
