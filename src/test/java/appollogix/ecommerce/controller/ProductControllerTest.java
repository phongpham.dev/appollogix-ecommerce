package appollogix.ecommerce.controller;

import appollogix.ecommerce.client.ElasticsearchClient;
import appollogix.ecommerce.config.ApplicationConfigTest;
import appollogix.ecommerce.controller.http.ProductController;
import appollogix.ecommerce.utils.JsonHelper;
import org.apache.commons.io.FileUtils;
import org.assertj.core.api.ObjectEnumerableAssert;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.collect.Tuple;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Spliterators;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.matchesPattern;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(
        ProductController.class
)
@Import(ApplicationConfigTest.class)
@ActiveProfiles({"default"})
public class ProductControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ElasticsearchClient esClient;

    @Autowired
    ResourceLoader resourceLoader;

    private final String indexName = "product";

    @BeforeEach
    public void initData() throws IOException, ExecutionException, InterruptedException {
        var resource = resourceLoader.getResource("classpath:databases/elasticsearch/data.json").getFile();
        var fileAsString = FileUtils.readFileToString(resource, "utf-8");
        var jsonTree = JsonHelper.getObjectMapper().readTree(fileAsString);
        jsonTree.fields().forEachRemaining(f -> {
            try {
                esClient.getClient().deleteByQuery(
                        new DeleteByQueryRequest(f.getKey()).setQuery(QueryBuilders.matchAllQuery()), RequestOptions.DEFAULT
                );

                var spliterator = Spliterators.spliteratorUnknownSize(f.getValue().at("/data").elements(), 0);
                var dataForTest = StreamSupport.stream(spliterator, false).map(source -> Tuple.tuple(UUID.randomUUID().toString(), source.toString())).toList();

                esClient.mIndex(f.getKey(), dataForTest, true, true).get();

            } catch (IOException | InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        });
        Thread.sleep(200);
    }


    @Test
    public void search_simple() throws Exception {
        var mvcResult = mockMvc.perform(
                        get("/product/search?page=2&size=5")
                )
                .andExpectAll(
                        status().isOk(),
                        request().asyncStarted()
                )
                .andReturn();

        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andDo(print())
                .andExpectAll(
                        jsonPath("total").value("10"),
                        jsonPath("data.*", hasSize(5))
                )
                .andReturn()
        ;
    }

    @Test
    public void search_ByKeyword_Success() throws Exception {
        var mvcResult = mockMvc.perform(
                        get("/product/search?keyword=macbook pro")
                )
                .andExpectAll(
                        status().isOk(),
                        request().asyncStarted()
                )
                .andReturn();

        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andDo(print())
                .andExpectAll(
                        jsonPath("total").value("4"),
                        jsonPath("data.*", hasSize(4))
                )
                .andReturn()
        ;
    }

    @Test
    public void search_Full_Success() throws Exception {
        var mvcResult = mockMvc.perform(
                        get("/product/search?page=1&size=5&keyword=macbook air&from_price=200&to_price=2000&colors=while,black&image=https://image.com.vn/1&category_ids=1,2")
                )
                .andExpectAll(
                        status().isOk(),
                        request().asyncStarted()
                )
                .andReturn();

        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andDo(print())
                .andExpect(jsonPath("total").value("1"))
                .andReturn()
        ;
    }
}
