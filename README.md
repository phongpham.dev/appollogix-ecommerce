
Step to start application:

Step 1. At root application
Step 2. run command: docker compose up -d

data sample(product) for search:

{"name":"macbook air","description":"Laptop 16GB, 256GB SSD","image":"https://image.com.vn/1","price":200,"colors":["red","blue","while"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"macbook air 15","description":"Laptop 16GB, 2TB SSD","image":"https://image.com.vn/1","price":200,"colors":["red","blue","while"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"macbook pro 13","description":"Laptop 8GB, 256GB SSD","image":"https://image.com.vn/2","price":300,"colors":["red"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"macbook pro 14","image":"https://image.com.vn/3","price":500,"colors":["blue"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"macbook pro 15","description":"Laptop 32GB, 256GB SSD","image":"https://image.com.vn/4","price":1000,"colors":["red","blue", "black"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"macbook pro 16","description":"Laptop 16GB, 1TB SSD","image":"https://image.com.vn/5","price":2000,"colors":["red","yellow"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"dell 1220","image":"https://image.com.vn/6","price":400,"colors":["red","black"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"dell 1555","description":"Laptop 4GB, 256GB SSD","image":"https://image.com.vn/7","price":500,"colors":["red","blue"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"asus vivobook","description":"Laptop 16GB, 256GB SSD","image":"https://image.com.vn/8","price":200,"colors":["red","blue"],"categories":[{"id": "1", "name":"laptop"}]},
{"name":"asus 1234","description":"Laptop 16GB, 256GB SSD","image":"https://image.com.vn/9","price":300,"colors":["red","blue"],"categories":[{"id": "1", "name":"laptop"}]}

search product api:

paginate search
curl --request GET "http://localhost:8080/product/search?page=1&size=3" | json_pp

search by keyword
curl --request GET "http://localhost:8080/product/search?keyword=macbook%20pro" | json_pp

search full field
curl --request GET "http://localhost:8080/product/search?page=1&size=5&keyword=macbook%20air&from_price=200&to_price=2000&colors=while,black&image=https://image.com.vn/1&category_ids=1,2" | json_pp

order api: 

create order
curl --request POST  http://localhost:8080/order \
     --header "Content-Type: application/json" \
     --data '{"customer_id":"phongpq","products":[{"product_id":"product id","quantity":5,"unit_price":10,"line_total":50}],"total_price":50,"has_vat":true}' | json_pp

get all order
curl --request GET  http://localhost:8080/order | json_pp
     
     


